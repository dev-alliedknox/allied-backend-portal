export const imeiValidator = (imei) => {
    var etal = /^[0-9]{15}$/;
    if (!etal.test(imei))
        return false;
    imei = imei.toString();
    let sum = 0; let mul = 2; let l = 14;
    for (let i = 0; i < l; i++) {
        let digit = imei.substring(l - i - 1, l - i);
        let tp = parseInt(digit, 10) * mul;
        if (tp >= 10)
            sum += (tp % 10) + 1;
        else
            sum += tp;
        if (mul == 1)
            mul++;
        else
            mul--;
    }
    let chk = ((10 - (sum % 10)) % 10);
    if (chk != parseInt(imei.substring(14, 15), 10))
        return false;
    return true;
}

export const cpfValidator = (cpf) => {
    let soma;
    let resto;
    soma = 0;
    cpf = cpf.toString();
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999") return false;

    for (let i = 1; i <= 9; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11)) resto = 0;
    if (resto != parseInt(cpf.substring(9, 10))) return false;

    soma = 0;
    for (let i = 1; i <= 10; i++) soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11)) resto = 0;
    if (resto != parseInt(cpf.substring(10, 11))) return false;
    return true;
}