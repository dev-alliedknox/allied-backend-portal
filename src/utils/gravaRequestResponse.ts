import { ClientDbTransaction } from '../clients/clientDbTransaction';
import TransactionRequest from '../models/TransactionRequest';


export async function gravaRequest(cpf: string, usuario: string, acao: string, mensagem: string, request: object, serialList: string[]) {                
    let transactionRequest = new TransactionRequest(cpf, usuario, acao, mensagem, request, serialList);    
    
    this.clientDbTransaction = new ClientDbTransaction(); 
    await this.clientDbTransaction.gravaRequest(transactionRequest); 
    return await this.clientDbTransaction.getTransactionId(transactionRequest); 
}

export async function gravaResponse(transactionId: number, response: string) {                
    this.clientDbTransaction = new ClientDbTransaction(); 
    await this.clientDbTransaction.gravaResponse(transactionId, response); 
}

export async function gravaResponseWithCompany(transactionId: number, response: string, idempresa) {                
    this.clientDbTransaction = new ClientDbTransaction(); 
    await this.clientDbTransaction.gravaResponseWithCompany(transactionId, response, idempresa); 
}