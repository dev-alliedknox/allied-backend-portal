import { Err } from 'allied-kernel';

class DbError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'database-error');
    }

} 

export { DbError };