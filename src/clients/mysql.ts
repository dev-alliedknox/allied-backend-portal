import { Request, Response } from 'express';
import * as mysql from 'mysql';
import request = require('request');

const conn = mysql.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    database: process.env.DBNAME,
});

const connect = async () => conn.connect();
const endConnection = async () => conn.end();

export const getAllHistoricoTransacao = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM historicoTransacao', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllStatusDispositivos = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM statusDispositivos', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllNotificacaoMensagens = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM notificacaoMensagens', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}
export const getAllLogNotificacoes = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM logNotificacoes', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllEstoqueLicensas = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT remainingQuantity as qtdDisponivel, activatedQuantity as licAtivadas, assignedQuantity as licRegistradas  FROM alliedknox_db.estoqueLicencas', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const postEstoqueLicencas = async (req: Request, res: Response) => {
    try {
        await connect();
        console.log(req.body);
        conn.query(`UPDATE alliedknox_db.estoqueLicencas SET qtdSeguranca = ${req.body.qtdSeguranca}, incrementoCompra = ${req.body.incrementoCompra}  WHERE id = ${req.body.id}`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}
export const getAllSerialWhitelist = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM alliedknox_db.serialWhitelist;', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const postSerialWhite = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`INSERT INTO alliedknox_db.serialWhitelist (serialNumber, ativo) VALUES (${req.body.serialNumber}, 1)`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllTagsMensagens = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM alliedknox_db.tagsMensagens', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const deleteSerialWhite = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`DELETE FROM alliedknox_db.serialWhitelist WHERE id = ${req.query.id}`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllLinxResellers = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM linxResellers', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}
export const getAllCadastroEmail = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM cadastroEmail', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getByImeiListaMensagens = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`SELECT cpf, date_format(dthora, '%m/%d/%Y %H:%i') as dthora, acao, mensagem 
        FROM alliedknox_db.knoxTransaction 
        WHERE serial like concat('%', '${req.body.imei}', '%')
        group by dthora`, async function (error, results, fields) {
            if(error) throw res.status(400).json(error);
            console.log(`The solution is: ${results}`);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }   
}

export const getAllTransacaoVarejista = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM transacaoVarejista', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllStatusCompraLicensa = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query("SELECT dthora, usuario, acao, mensagem FROM alliedknox_db.knoxTransaction WHERE acao like '%SLM%'", async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const postCadastroEmail = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`INSERT INTO alliedknox_db.cadastroEmail (nome, email) VALUES ('${req.body.nome}', '${req.body.email}')`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllLinxMovimentoSerial = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query("select count(id) licRegistradas from alliedknox_db.linxMovimentoSerial where status_kg in ('Activating', 'Active')", async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const postListaMensagens = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`INSERT INTO alliedknox_db.listaMensagens ( mensagem, tipomensagem, idlinxresellers) values ('${req.body.mensagem}', '${req.body.tipomensagem}', ${req.body.idlinxresellers})`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const getAllListaMensagens = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query('SELECT * FROM alliedknox_db.listaMensagens', async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}

export const postUnionMensagensTags = async (req: Request, res: Response) => {
    try {
        await connect();
        conn.query(`INSERT INTO alliedknox_db.unionMensagensTags (idListaMensagens, idTagsMensagens) values (${req.body.idListaMensagens},${req.body.idTagsMensagens})`, async function (error, results, fields) {
            if (error) throw res.status(400).json(error);
            console.log('The solution is: ', results);
            await endConnection();
            res.status(200).json(results);
        });
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
}
