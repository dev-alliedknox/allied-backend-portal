import { KnexSingleton } from '../utils/KnexSingleton';
import { DbError } from '../errors/DbError';
import { isNullOrUndefined } from 'util';

class ClientDb {

    private conn = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || "3306"), process.env.DBPASSWORD, process.env.DBNAME).conn;

    public constructor() { }


    public getAllHistoricoTransacao = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){               
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);                        
            return await this.conn.select('*')
                .from('historicoTransacao')                
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });           
        }
        return await this.conn.select('*')
            .from('historicoTransacao')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllStatusDispositivos = async (data) => {
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('statusDispositivos')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('statusDispositivos')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllNotificacaoMensagens = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('notificacaoMensagens')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('notificacaoMensagens')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllLogNotificacoes = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('logNotificacoes')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });        
        }
        return await this.conn.select('*')
            .from('logNotificacoes')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllEstoqueLicensas = async (data) => {
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn
                .select('remainingQuantity as qtdDisponivel', 'activatedQuantity as licAtivadas', 'assignedQuantity as licRegistradas')
                .from('estoqueLicencas')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });        
        }
        return await this.conn
            .select('remainingQuantity as qtdDisponivel', 'activatedQuantity as licAtivadas', 'assignedQuantity as licRegistradas')
            .from('estoqueLicencas')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postEstoqueLicencas = async (data) => {
        //if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
            return await this.conn('estoqueLicencas')
                .update({ qtdSeguranca: data.qtdSeguranca, incrementoCompra: data.incrementoCompra })
                .whereIn('idempresa', data.idempresa)
                .andWhere('id', '=', 1) 
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        //}
        return await this.conn('estoqueLicencas')
            .update({ qtdSeguranca: data.qtdSeguranca, incrementoCompra: data.incrementoCompra })
            .where('id', '=', 1)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllSerialWhitelist = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('serialWhitelist')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('serialWhitelist')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postSerialWhite = async (data) => {
        /*
        if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
            return await this.conn('serialWhitelist')
                .whereIn('idempresa', data.idempresa)
                .insert({ serialNumber: data.serialNumber, ativo: 1 })                
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        } */
        return await this.conn('serialWhitelist')
            .insert({ serialNumber: data.serialNumber, ativo: 1, idempresa: data.idempresa })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllTagsMensagens = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('tagsMensagens')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });        
        }
        return await this.conn.select('*')
            .from('tagsMensagens')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    
    public deleteSerialWhite = async (data) => {
        if (data.idempresa){   
            let IdEmpresa: number[] = JSON.parse(data.idempresa)        
            if (!isNullOrUndefined(IdEmpresa) && IdEmpresa.length > 0){ 
                return await this.conn('serialWhitelist')
                    .whereIn('idempresa', IdEmpresa)
                    .andWhere('id', '=', data.id)
                    .del()
                    .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
            }
        }  
        return await this.conn('serialWhitelist')
            .where('id', '=', data.id)
            .del()
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllLinxResellers = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('linxResellers')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('linxResellers')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllCadastroEmail = async (data) => {
        console.log('DATA: ', data); 
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('cadastroEmail')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('cadastroEmail')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getByImeiListaMensagens = async (data) => {   
        console.log('DATA: ', data);     
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn        
                .select(this.conn.raw("linxMovimentoSerial.serial, linxFaturamento.cpf_cnpj as cpf, date_format(knoxTransaction.dthora, '%d/%m/%Y %H:%i') as dthora, knoxTransaction.acao, knoxTransaction.mensagem, knoxTransaction.usuario"))
                .whereIn('knoxTransaction.idempresa', IdEmpresa)
                .andWhereRaw("linxFaturamento.transacao = linxMovimentoSerial.transacao")                
                .andWhereRaw("knoxTransaction.serial like concat('%', linxMovimentoSerial.serial, '%')")
                .andWhereRaw(`(linxMovimentoSerial.serial = ${data.imei} or linxFaturamento.cpf_cnpj = ${data.imei} )`)                        
                .orderBy('linxMovimentoSerial.serial')
                .orderBy('knoxTransaction.dthora', 'desc')
                .from(this.conn.raw('linxFaturamento, linxMovimentoSerial, knoxTransaction'))
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        }
        return await this.conn        
            .select(this.conn.raw("linxMovimentoSerial.serial, linxFaturamento.cpf_cnpj as cpf, date_format(knoxTransaction.dthora, '%d/%m/%Y %H:%i') as dthora, knoxTransaction.acao, knoxTransaction.mensagem, knoxTransaction.response, knoxTransaction.usuario"))
            .whereRaw("linxFaturamento.transacao = linxMovimentoSerial.transacao")
            .andWhereRaw("knoxTransaction.serial like concat('%', linxMovimentoSerial.serial, '%')")
            .andWhereRaw(`(linxMovimentoSerial.serial = ${data.imei} or linxFaturamento.cpf_cnpj = ${data.imei} )`)
            .orderBy('linxMovimentoSerial.serial')
            .orderBy('knoxTransaction.dthora', 'desc')
            .from(this.conn.raw('linxFaturamento, linxMovimentoSerial, knoxTransaction'))
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllTransacaoVarejista = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn.select('*')
                .from('transacaoVarejista')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('transacaoVarejista')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllStatusCompraLicensa = async (data) => {
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
                return await this.conn
                    .select('dthora', 'usuario', 'acao', 'mensagem')
                    .whereRaw(`idempresa in (${IdEmpresa})`)
                    .andWhereRaw("acao like '%SLM%'")
                    .from('knoxTransaction')
                    .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn
            .select('dthora', 'usuario', 'acao', 'mensagem')
            .whereRaw("acao like '%SLM%'")
            .from('knoxTransaction')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postCadastroEmail = async (data) => {
        return await this.conn('cadastroEmail')
            .insert({ nome: data.nome, email: data.email, idempresa: data.idempresa })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public putCadastroEmail = async (data) => {
        console.log('EMAILDATA: ', data)
        return await this.conn('cadastroEmail')
            .update({ nome: data.nome, email: data.email, idempresa: data.idempresa })
            .where('id', '=', data.id)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    
    public deleteCadastroEmail = async (data) => {  
        if (data.idempresa){   
            let IdEmpresa: number[] = JSON.parse(data.idempresa)        
            if (!isNullOrUndefined(IdEmpresa) && IdEmpresa.length > 0){ 
                return await this.conn('cadastroEmail')
                    .whereIn('idempresa', IdEmpresa)
                    .andWhere('id', '=', data.id)
                    .del()
                    .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
            }
        }      
        return await this.conn('cadastroEmail')
            .where('id', '=', data.id)
            .del()
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllLinxMovimentoSerial = async (data) => {
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa); 
            return await this.conn
                .count('linxMovimentoSerial.id as licRegistradas')
                .whereRaw(`linxMovimentoSerial.idempresa in (${IdEmpresa})`)                
                .andWhereRaw("linxMovimentoSerial.status_kg in ('Accepted', 'Activating', 'Enrolled', 'Active')")
                .from(this.conn.raw('linxMovimentoSerial'))
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn
            .count('id as licRegistradas')
            .whereRaw("status_kg in ('Activating', 'Active')")
            .from('linxMovimentoSerial')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postListaMensagens = async (data) => {
        /*
        if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
            return await this.conn('listaMensagens')
                .whereIn('idempresa', data.idempresa)
                .insert({ mensagem: data.mensagem, Ativo: data.Ativo, DtAdd: data.DtAdd, tipomensagem: data.tipomensagem, idempresa: data.idempresa })
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        } */
        return await this.conn('listaMensagens') 
            .insert({ mensagem: data.mensagem, Ativo: data.Ativo, DtAdd: data.DtAdd, tipomensagem: data.tipomensagem, idempresa: data.idempresa })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public putListaMensagens = async (data) => {
        if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
            return await this.conn('listaMensagens')
                .whereIn('idempresa', data.idempresa)
                .andWhere('id', '=', data.id)
                .update({ mensagem: data.mensagem, Ativo: data.Ativo, DtUpd: data.DtUpd, tipomensagem: data.tipomensagem, idempresa: data.idempresa })
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn('listaMensagens')
            .where('id', '=', data.id)
            .update({ mensagem: data.mensagem, Ativo: data.Ativo, DtUpd: data.DtUpd, tipomensagem: data.tipomensagem, idempresa: data.idempresa })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    

    public getAllListaMensagens = async (data) => {
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);          
            return await this.conn.select('*')
                .from('listaMensagens')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaMensagens')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postUnionMensagensTags = async (data) => {
        /*
        if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
            return await this.conn('unionMensagensTags')
                .whereIn('idempresa', data.idempresa)
                .insert({ idListaMensagens: data.idListaMensagens, idTagsMensagens: data.idTagsMensagens })
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        } */
        return await this.conn('unionMensagensTags')
            .insert({ idListaMensagens: data.idListaMensagens, idTagsMensagens: data.idTagsMensagens, idempresa: data.idempresa })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    // public putUnionMensagensTags = async (data) => {
    //     /*
    //     if (!isNullOrUndefined(data.idempresa) && data.idempresa.length > 0){
    //         return await this.conn('unionMensagensTags')
    //             .whereIn('idempresa', data.idempresa)
    //             .andWhere('id', '=', data.id)
    //             .update({ idListaMensagens: data.idListaMensagens, idTagsMensagens: data.idTagsMensagens })
    //             .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });        
    //     } */
    //     return await this.conn('unionMensagensTags')
    //         .where('id', '=', data.id)
    //         .update({ idListaMensagens: data.idListaMensagens, idTagsMensagens: data.idTagsMensagens, idempresa: data.idempresa })
    //         .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    // }

    public getUnionMensagensTags = async (data) => {
        // console.log('getUnionMensagensTags-DATA: ', data);
        const ID = Number(data.id);        
        return await this.conn.select('*')
            .from('unionMensagensTags')
            .where('idListaMensagens', '=', ID)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public postResellerVarejista = async (data) => {
        return await this.conn('linxResellers')
            .whereIn('idempresa', data.idempresa)
            .insert({ idempresa: data.idempresa, razao_social: data.razao_social, 
                nome_fantasia: data.nome_fantasia, cnpj: data.cnpj, email: data.email, 
                telefone: data.telefone, nome: data.nome,ativo:data.ativo })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public putResellerVarejista = async (data) => {
        return await this.conn('linxResellers')
            // .whereIn('idempresa', data.idempresa)
            .where('id', '=', data.id)
            //.andWhere('id', '=', data.id)
            .update({ idempresa: data.idempresa, razao_social: data.razao_social, 
                        nome_fantasia: data.nome_fantasia, cnpj: data.cnpj, email: data.email, 
                        telefone: data.telefone, nome: data.nome,ativo:data.ativo })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', err)  });
    }


    public getAllListaBloqueados = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')                
                .from('listaBloqueados')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaBloqueados')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllListaBlinkados = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')                
                .from('listaBlinkados')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaBlinkados')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllListaAtivos = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')                
                .from('listaAtivos')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaAtivos')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllListaImeiManual = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')                
                .from('listaImeiManual')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaImeiManual')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getAllListaQuitados = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')                
                .from('listaQuitados')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn.select('*')
            .from('listaQuitados')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }


    public getTransactionId = async (data) => { 
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn 
                .select(this.conn.raw("max(id) as transactionId"))
                .from('knoxTransaction')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });            
        }
        return await this.conn 
            .select(this.conn.raw("max(id) as transactionId"))
            .from('knoxTransaction')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
/*
    public getBuscaPin = async (data) => {        
        return await this.conn            
            .select('knoxTransaction.response')
            .from('knoxTransaction')
            .whereRaw(`knoxTransaction.serial like concat('%', ${data.serial}, '%')`)
            .andWhere('knoxTransaction.acao', 'GetPin')
            .orderBy('knoxTransaction.id', 'desc')
            .limit(1)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
*/
    public postGravaRequest = async (TransactionRequest: object) => {   
        delete TransactionRequest['type'];
        //console.log('TRANSACION: '); console.log(TransactionRequest); 
        await this.conn('knoxTransaction')
            .insert(TransactionRequest);    
            //.then(await this.getTransId(TransactionRequest));
        return await this.getTransId(TransactionRequest);
    }

    public getTransId = async (TransactionRequest: any) => {        
        const dthora = TransactionRequest.dthora;
        const usuario = TransactionRequest.usuario;
        console.log('TRANSACION ID'); 
        return await this.conn
            .select('knoxTransaction.id')
            .from('knoxTransaction')                                   
            .where('knoxTransaction.dthora', dthora)                        
            .andWhere('knoxTransaction.usuario', usuario);        
    }

    public postGravaResponse = async (data: any) => {
        await this.conn('knoxTransaction')
            .where('id', '=', data.transactionId)
            .update('response', data.response);
    }

    public putUpdateStatusKg = async (data: any) => {        
        await this.conn('linxMovimentoSerial')
            .where('serial', '=', data.serial)
            .update('status_kg', data.status);
    }

    public getRelacionamentoSoudi = async () => { 
        return await this.conn.select('*')
            .from('relacionamentoSoudi')
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public putRelacionamentoSoudi = async (soudi: any) => {  
        delete soudi['type'];      
        console.log('SOUDI: ', soudi)
        await this.conn('relacionamentoSoudi')
            .where('id', '=', soudi.id)
            .update(soudi)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
    
    public putRemoveMovSerial = async (data) => {
        delete data['type'];      
        console.log('SOUDI: ', data)
        await this.conn('linxMovimentoSerial')
            .where('serial', '=', data.imei)
            .update({ status_kdp: 'Desassociado', status_kg: 'Desassociado' })
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public getLinxResselerByCnpj = async (cnpj) => {
        return await this.conn.select('*')
            .from('linxResellers')
            .where('cnpj', '=', cnpj)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public getLinxMovimentoSerialBySerial = async (serial) => {
        return await this.conn.select('*')
            .from('linxMovimentoSerial')
            .where('serial', '=', serial)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public getVendaManual = async (data) => {
        console.log('DATA: ', data);
        if (data.idempresa){   
            let IdEmpresa = data.idempresa.split(',').map(Number);
            console.log('IdEmpresa: ', IdEmpresa);  
            return await this.conn.select('*')
                .from('linxFaturamento')
                .innerJoin('linxMovimentoSerial', 'linxFaturamento.transacao', '=', 'linxMovimentoSerial.transacao')
                .whereRaw(`idempresa in (${IdEmpresa})`)
                .andWhere('isVendaManual', '=', true)
                .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
        }
        return await this.conn.select('*')
            .from('linxFaturamento')
            .innerJoin('linxMovimentoSerial', 'linxFaturamento.transacao', '=', 'linxMovimentoSerial.transacao')
            .where('isVendaManual', '=', true)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public getNextFaturamento = async () => {
        return await this.conn.select('AUTO_INCREMENT')
            .from('information_schema.TABLES')
            .where('TABLE_SCHEMA', '=', 'alliedknox_db')
            .andWhere('TABLE_NAME', '=', 'linxFaturamento');
    }

    public postVendaManualMovimentoSerial = async (item) => {
        return await this.conn('linxMovimentoSerial')
            .insert(item)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public postVendaManualFaturamento = async (item) => {
        return await this.conn('linxFaturamento')
            .insert(item)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public putVendaManualMovimentoSerial = async (item) => {
        return await this.conn('linxMovimentoSerial')
            .where('transacao', '=', item.transacao)
            .update(item)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }

    public putVendaManualFaturamento = async (item) => {
        return await this.conn('linxFaturamento')
            .where('transacao', '=', item.transacao)
            .update(item)
            .catch((err) => { throw new DbError('Falha ao acessar banco de dados.', 500) });
    }
}

export { ClientDb }