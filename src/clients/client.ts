import { HttpClient } from 'allied-kernel';

class Client {

    private httpClient: HttpClient;

    public constructor() {

        this.httpClient = new HttpClient({
            
            baseURL: process.env.BASE_URL_CLIENT,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    public async getOk(): Promise<any> {

        return await this.httpClient.get('/');
    }


    public async sendStatus(data: any): Promise<any> {

        const response = await this.httpClient.post('/', data);
        return response.data;
    }

    public async updateStatus(data: any): Promise<any> {

        const response = await this.httpClient.put('/', data);
        return response.data;
    }
}

export default Client;