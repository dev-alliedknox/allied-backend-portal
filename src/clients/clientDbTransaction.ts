import { KnexSingleton } from "../utils/KnexSingleton";

class ClientDbTransaction {
    private connection;
    constructor() {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public gravaRequest = async (TransactionRequest: object) => {        
        await this.connection('knoxTransaction')
            .insert(TransactionRequest)    
            //.then(this.getTransactionId(TransactionRequest));
    }

    public getTransactionId = async (TransactionRequest: any) => {        
        const dthora = TransactionRequest.dthora;
        const usuario = TransactionRequest.usuario;
        return await this.connection.select('knoxTransaction.id')
            .from('knoxTransaction')                                   
            .where('knoxTransaction.dthora', dthora)                        
            .andWhere('knoxTransaction.usuario', usuario);        
    }

    public gravaResponse = async (transactionId: number, TransactionResponse: object) => {
        await this.connection('knoxTransaction')
            .where('id', '=', transactionId)
            .update('response', TransactionResponse);
    }

    public gravaResponseWithCompany = async (transactionId: number, TransactionResponse: object, idempresa: string) => {
        await this.connection('knoxTransaction')
            .where('id', '=', transactionId)
            .update({
                response: TransactionResponse,
                idempresa: idempresa
            });
    }
}

export { ClientDbTransaction }