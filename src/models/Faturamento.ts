export default class Faturamento {

    constructor(
        readonly nome_cliente: string,
        readonly empresa: number,
        readonly cnpj: string,
        readonly razao_social: string,
        readonly nome_fantasia: string,
        readonly nota_fiscal: string,
        readonly chave_nfe: string,
        readonly modelo_nf: string,
        readonly cpf_cnpj: string,
        readonly data_emissao: string,
        readonly total_liquido: number,
        readonly nome_plano_cartao_1: string,
        readonly parcelas_plano_cartao_1: number,
        readonly categoria: string,
        readonly transacao: number,
        readonly isVendaManual = true
    ) {}
}
