import IValidator from "../interfaces/IValidator";
import { cpfValidator, imeiValidator } from "../utils/validatorUtils";
import { ValidateError } from "../errors/ValidateError";
import { isNumber } from "util";

class VendaManual implements IValidator {
    public cnpj: string;
    public razaoSocial: string;
    public nomeLoja: string;
    public numeroLoja: number;
    public nota: string;
    public chaveNfe: string;
    public modeloDocumento: string;
    public numeroVenda: number;
    public linhaNota: number;
    public cpfCliente: string;
    public dataEmissao: string;
    public valor: number;
    public formaPagamento: string;
    public parcelas: number;
    public tipoDispositivo: string;
    public imeiSn: string;
    public transacao: number;
    public idempresa: number;
    public nomeCliente: string;

    constructor(data) {

        this.cnpj = data.cnpj;
        this.razaoSocial = data.razaoSocial;
        this.nomeLoja = data.nomeLoja;
        this.numeroLoja = data.numeroLoja;
        this.nota = data.nota;
        this.chaveNfe = data.chaveNfe;
        this.modeloDocumento = data.modeloDocumento;
        this.numeroVenda = data.numeroVenda;
        this.linhaNota = data.linhaNota;
        this.cpfCliente = data.cpfCliente;
        this.dataEmissao = data.dataEmissao;
        this.valor = data.valor;
        this.formaPagamento = data.formaPagamento;
        this.parcelas = data.parcelas;
        this.tipoDispositivo = data.tipoDispositivo;
        this.imeiSn = data.imeiSn;
        this.transacao = data.transacao;
        this.idempresa = data.idempresa;
        this.nomeCliente = data.nomeCliente;

        this.validate();
    }

    public validate  = () => {

        if (!this.cnpj) throw new ValidateError("CNPJ is required.");
        
        if (!this.razaoSocial) throw new ValidateError("razaoSocial is required.");

        if (!this.nota) throw new ValidateError("nota is required.");

        if (!this.modeloDocumento) throw new ValidateError("modeloDocumento is required.");

        if (!this.cpfCliente) throw new ValidateError("CPF is required.");
               
        if (!cpfValidator(this.cpfCliente)) throw new ValidateError("CPF is not valid.");

        if (!this.dataEmissao) throw new ValidateError("dataEmissao is required.");

        if (!this.valor) throw new ValidateError("valor is required.");

        if (!this.formaPagamento) throw new ValidateError("formaPagamento is required.");

        if (!this.parcelas) throw new ValidateError("parcelas is required.");

        if (!this.tipoDispositivo) throw new ValidateError("tipoDispositivo is required.");
        
        if (this.imeiSn && isNumber(this.imeiSn) && !imeiValidator(this.imeiSn)) 
            throw new ValidateError("IMEI is not valid.");
    }
}

export { VendaManual };