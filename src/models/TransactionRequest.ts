
class TransactionRequest {  
  public dthora: string;
  public cpf: string;
  public usuario: string;
  public acao: string;
  public mensagem: string;
  public request: string;     
  public serial: string;    

  
  public constructor(cpf: string, usuario: string, acao: string, mensagem: string,  request: object, serialList: string[]) {    
    this.dthora = this.getDate();
    this.cpf = cpf;
    this.usuario = usuario;
    this.acao = acao;
    this.mensagem = mensagem;
    this.request = JSON.stringify(request);    
    this.serial = JSON.stringify(serialList);
  }

  private getDate(): string  { 
    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    
    return date+' '+time;
  }

}

export default TransactionRequest;