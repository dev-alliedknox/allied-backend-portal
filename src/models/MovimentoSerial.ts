export default class MovimentoSerial {

    constructor(
        readonly cnpj_emp: string,
        readonly serial: string,
        readonly transacao: number,
        readonly idempresa: number
    ) {}
}
