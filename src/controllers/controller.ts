import { Request, Response } from 'express';
import Service from '../services/service';
import Client from '../clients/client';
import { ClientDb } from '../clients/clientDb';
import * as xlsx from 'xlsx';
import { VendaManual } from '../models/VendaManual';
import { gravaRequest, gravaResponseWithCompany } from '../utils/gravaRequestResponse';


export const showOk = async (req: Request, res: Response)  => {    
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getOk();
    res.json(response);
} 


export const getHistoricoTransacao = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllHistoricoTransacao(req.query);
    console.log(response);
    res.status(200).json(response); 
}


export const getStatusDispositivos = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllStatusDispositivos(req.query);
    console.log(response);
    res.status(200).json(response); 
}


export const getNotificacaoMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllNotificacaoMensagens(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getLogNotificacoes = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllLogNotificacoes(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const postEstoqueLicencas = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postEstoqueLicencas(req.body);
    console.log(response);
    res.status(200).json(response);
}


export const getAllSerialWhitelist = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllSerialWhitelist(req.query);
    console.log(response);
    res.status(200).json(response); 
}


export const postSerialWhite = async (req: Request,res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postSerialWhite(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const deleteSerialWhite = async (req: Request,res: Response) => {
    const service = new Service(new Client(), new ClientDb());    
    const response = await service.deleteSerialWhite(req.query);
    console.log(response);
    res.status(204).json(response);
}


export const getAllLinxResellers = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllLinxResellers(req.query);
    console.log(response);
    res.status(200).json(response);  
}


export const getAllCadastroEmail = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllCadastroEmail(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getByImeiListaMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getByImeiListaMensagens(req.body);
    console.log(response);
    res.status(200).json(response);
}


export const getAllTransacaoVarejista = async(req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllTransacaoVarejista(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllStatusCompraLicensa = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllStatusCompraLicensa(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const postCadastroEmail = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postCadastroEmail(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const putCadastroEmail = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.putCadastroEmail(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const deleteCadastroEmail = async (req: Request,res: Response) => {
    const service = new Service(new Client(), new ClientDb());    
    const response = await service.deleteCadastroEmail(req.query);
    console.log(response);
    res.status(204).json(response);
}


export const getAllLinxMovimentoSerial = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllLinxMovimentoSerial(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllEstoqueLicensas = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllEstoqueLicensas(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllTagsMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllTagsMensagens(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const postListaMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postListaMensagens(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const putListaMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.putListaMensagens(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const postUnionMensagensTags = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postUnionMensagensTags(req.body);
    console.log(response);
    res.status(201).json(response);
}


// export const putUnionMensagensTags = async (req: Request, res: Response) => {
//     const service = new Service(new Client(), new ClientDb());
//     const response = await service.putUnionMensagensTags(req.body);
//     console.log(response);
//     res.status(201).json(response);
// }

export const getUnionMensagensTags = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getUnionMensagensTags(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const postResellerVarejista = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postResellerVarejista(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const putResellerVarejista = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.putResellerVarejista(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const getAllListaMensagens = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());      
    const response = await service.getAllListaMensagens(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllListaBloqueados = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllListaBloqueados(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllListaBlinkados = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllListaBlinkados(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllListaAtivos = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllListaAtivos(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllListaImeiManual = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllListaImeiManual(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getAllListaQuitados = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getAllListaQuitados(req.query);
    console.log(response);
    res.status(200).json(response);
}


export const getTransactionId = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getTransactionId(req.query);
    console.log(response);
    res.status(200).json(response);
}
/*
export const getBuscaPin = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getBuscaPin(req.body);
    console.log(response);
    res.status(201).json(response);
}
*/
export const postGravaRequest = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postGravaRequest(req.body);
    console.log(response);
    res.status(201).json(response);
}

export const postGravaResponse = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.postGravaResponse(req.body);
    console.log(response);
    res.status(201).json(response);
}

export const putUpdateStatusKg = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.putUpdateStatusKg(req.body);
    console.log(response);
    res.status(201).json(response);
}

export const getRelacionamentoSoudi = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getRelacionamentoSoudi();
    console.log(response);
    res.status(200).json(response);
}


export const putRelacionamentoSoudi = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.putRelacionamentoSoudi(req.body);
    console.log(response);
    res.status(201).json(response);
}


export const putRemoveMovSerial = async (req: Request,res: Response) => {
    const service = new Service(new Client(), new ClientDb());    
    const response = await service.putRemoveMovSerial(req.body);
    console.log(response);
    res.status(204).json(response);
}


export const sendStatus = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    let data = req.body;
    const response = await service.sendStatus(data);
    console.log(response);
    res.status(201).json(response);
}


export const updateStatus = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    let data = req.body;
    const response = await service.updateStatus(data);
    res.status(200).json(response);
}

export const getVendaManual = async (req: Request, res: Response) => {
    const service = new Service(new Client(), new ClientDb());
    const response = await service.getVendaManual(req.query);
    console.log(response);
    res.status(200).json(response);
}

export const postVendaManualSpreadSheet = async (req, res) => {
    const workbook = await xlsx.read(req.files.vendas.data, {type:"buffer", cellDates: true});
    const sheet_name_list = workbook.SheetNames;
    
    let lista: VendaManual[] = await xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]], 
        // { header:[
        //     "cnpj","razaoSocial","nomeLoja","numeroLoja",
        //     "nota","chaveNfe","modeloDocumento","numeroVenda",
        //     "linhaNota","cpfCliente","dataEmissao","valor",
        //     "formaPagamento","parcelas","tipoDispositivo","imeiSn"
        // ]});
        { header:[
            "cnpj","razaoSocial","nomeLoja","numeroLoja",
            "nota","linhaNota","modeloDocumento","chaveNfe",
            "nomeCliente","cpfCliente","dataEmissao","valor",
            "formaPagamento","parcelas","tipoDispositivo","imeiSn"
        ]});
        //console.log('LISTA: ', lista)
    const service = new Service(new Client(), new ClientDb());

    let response = { 
        count: 0, 
        success: 0, 
        fail: 0, 
        resultlist: new Array<any>()
    };

    for (let i = 0; i < lista.length; i++) {
        let transactionId = await gravaRequest(lista[i].cpfCliente, req.username, 'postVendaManual', `excel - ${req.files.vendas.name} - item: ${i+1}`,lista[i], [lista[i].imeiSn]);
        let logResponse;
        let idempresa;
        await service.postVendaManual(lista[i])
            .then((venda) => {
                logResponse = JSON.stringify(venda);
                idempresa = venda.idempresa;
                response.success++ 
            }).catch( err => { 
                logResponse = err.message;
                response.fail++;
                response.resultlist.push({ error: { item: i + 1 , message: err.message }} );
            });            
        await gravaResponseWithCompany(transactionId[0].id, logResponse, idempresa);
        response.count++;
    }

    console.log(response);
    res.status(200).json(response);
}

export const postVendaManual = async (req: any, res: Response) => {

    const service = new Service(new Client(), new ClientDb());
    let lista = req.body;

    let response = { 
        count: 0, 
        success: 0, 
        fail: 0, 
        resultlist: new Array<any>()
    };

    for (let i = 0; i < lista.length; i++) {
        let transactionId = await gravaRequest(lista[i].cpfCliente, req.username, 'postVendaManual', `item: ${i+1}`,lista[i], [lista[i].imeiSn]);
        let logResponse;
        let idempresa;
        await service.postVendaManual(lista[i])
            .then((venda) => {
                logResponse = JSON.stringify(venda);
                idempresa = venda.idempresa;
                response.success++ 
            }).catch( err => { 
                logResponse = err.message;
                response.fail++;
                response.resultlist.push({ error: { item: i + 1 , message: err.message }} );
            }); 
        await gravaResponseWithCompany(transactionId[0].id, logResponse, idempresa);
        response.count++;
    }

    console.log(response);
    res.status(200).json(response);
}

export const putVendaManual = async (req: any, res: Response) => {

    const service = new Service(new Client(), new ClientDb());
    let lista = req.body;

    let response = { 
        count: 0, 
        success: 0, 
        fail: 0, 
        resultlist: new Array<any>()
    };

    for (let i = 0; i < lista.length; i++) {
        let transactionId = await gravaRequest(lista[i].cpfCliente, req.username, 'putVendaManual', `item: ${i+1}`,lista[i], [lista[i].imeiSn]);
        let logResponse;
        let idempresa;
        await service.putVendaManual(lista[i])
            .then((venda) => {
                logResponse = JSON.stringify(venda);
                idempresa = venda.idempresa;
                response.success++ 
            }).catch( err => { 
                logResponse = err.message;
                response.fail++;
                response.resultlist.push({ error: { item: i + 1 , message: err.message }} );
            });
        await gravaResponseWithCompany(transactionId[0].id, logResponse, idempresa);
        response.count++;
    }

    console.log(response);
    res.status(200).json(response);
}

