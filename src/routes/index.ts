import { Router } from 'express';
import * as controller from '../controllers/controller';
import { asyncMiddleware } from 'allied-kernel';
import { swaggerSetup, swaggerUi } from '../configs/swagger';
import * as cors from 'cors';
import * as fileUpload from 'express-fileupload';
import { verifyJWT } from '../utils/jwtValidation';

const router = Router();

router.options('*', cors());
//router.use('/', cors())

router.get('/', asyncMiddleware(controller.showOk));
router.post('/', asyncMiddleware(controller.sendStatus));
router.put('/', asyncMiddleware(controller.updateStatus));
router.get('/historicoTransacao', asyncMiddleware(controller.getHistoricoTransacao));
router.get('/statusDispositivos', asyncMiddleware(controller.getStatusDispositivos));
router.get('/notificacaoMensagens', asyncMiddleware(controller.getNotificacaoMensagens));
router.get('/logNotificacoes', asyncMiddleware(controller.getLogNotificacoes));
router.post('/estoqueLicencas', asyncMiddleware(controller.postEstoqueLicencas));
router.get('/estoqueLicencas', asyncMiddleware(controller.getAllEstoqueLicensas));
router.get('/serialWhitelist', asyncMiddleware(controller.getAllSerialWhitelist));
router.post('/serialWhitelist', asyncMiddleware(controller.postSerialWhite));
router.delete('/serialWhitelist', asyncMiddleware(controller.deleteSerialWhite));
router.get('/linxResellers', asyncMiddleware(controller.getAllLinxResellers));
router.get('/cadastroEmail', asyncMiddleware(controller.getAllCadastroEmail));
router.post('/imeiMensagens', asyncMiddleware(controller.getByImeiListaMensagens));
router.get('/transacaoVarejista', asyncMiddleware(controller.getAllTransacaoVarejista));
router.get('/StatusCompraLicensa', asyncMiddleware(controller.getAllStatusCompraLicensa));
router.get('/linxMovimentoSerial', asyncMiddleware(controller.getAllLinxMovimentoSerial))
router.get('/estoqueLicensas', asyncMiddleware(controller.getAllEstoqueLicensas));
router.post('/CadastroEmail', asyncMiddleware(controller.postCadastroEmail));
router.put('/CadastroEmail', asyncMiddleware(controller.putCadastroEmail));
router.delete('/cadastroEmail', asyncMiddleware(controller.deleteCadastroEmail));
router.get('/tagsMensagens', asyncMiddleware(controller.getAllTagsMensagens));
router.post('/listaMensagens',asyncMiddleware(controller.postListaMensagens));
router.put('/listaMensagens',asyncMiddleware(controller.putListaMensagens));
router.get('/listaMensagens',asyncMiddleware(controller.getAllListaMensagens));
router.post('/unionMensagensTags', asyncMiddleware(controller.postUnionMensagensTags));
//router.put('/unionMensagensTags', asyncMiddleware(controller.putUnionMensagensTags));
router.get('/unionMensagensTags', asyncMiddleware(controller.getUnionMensagensTags));
router.post('/resellerVarejista', asyncMiddleware(controller.postResellerVarejista));
router.put('/resellerVarejista', asyncMiddleware(controller.putResellerVarejista));
router.get('/listaBloqueados',asyncMiddleware(controller.getAllListaBloqueados));
router.get('/listaBlinkados',asyncMiddleware(controller.getAllListaBlinkados));
router.get('/listaAtivos',asyncMiddleware(controller.getAllListaAtivos));
router.get('/listaImeiManual',asyncMiddleware(controller.getAllListaImeiManual));
router.get('/listaQuitados',asyncMiddleware(controller.getAllListaQuitados));
router.get('/transactionId',asyncMiddleware(controller.getTransactionId));
router.post('/gravaRequest', asyncMiddleware(controller.postGravaRequest));
router.post('/gravaResponse', asyncMiddleware(controller.postGravaResponse));
router.put('/updateStatusKg',asyncMiddleware(controller.putUpdateStatusKg));
router.get('/relacionamentoSoudi',asyncMiddleware(controller.getRelacionamentoSoudi));
router.put('/relacionamentoSoudi',asyncMiddleware(controller.putRelacionamentoSoudi));
router.put('/removeMovSerial', asyncMiddleware(controller.putRemoveMovSerial));

router.use('/vendaManual', verifyJWT);
router.get('/getVendaManual', asyncMiddleware(controller.getVendaManual));
router.post('/vendaManual', asyncMiddleware(controller.postVendaManual));
router.put('/vendaManual', asyncMiddleware(controller.putVendaManual));

router.use('/vendaManual/SpreadSheet', fileUpload());
router.post('/vendaManual/SpreadSheet', asyncMiddleware(controller.postVendaManualSpreadSheet));

router.use('/', swaggerUi.serve);
router.get('/api-docs', swaggerSetup);

export default router;