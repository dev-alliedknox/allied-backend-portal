import  Client from '../clients/client';
import { ClientDb } from '../clients/clientDb';
import { VendaManual } from '../models/VendaManual';
import Faturamento from '../models/Faturamento';
import MovimentoSerial from '../models/MovimentoSerial';
import { isNullOrUndefined } from 'util';

class Service {

    private client: Client;
    private clientDb: ClientDb;

    public constructor(client: Client, clientDb: ClientDb) {
        this.client = client;        
        this.clientDb = clientDb;
    }


    public getAllHistoricoTransacao = async (data) =>  await this.clientDb.getAllHistoricoTransacao(data);

    public getAllStatusDispositivos = async (data) =>  await this.clientDb.getAllStatusDispositivos(data);

    public getAllNotificacaoMensagens = async (data) =>  await this.clientDb.getAllNotificacaoMensagens(data);

    public getAllLogNotificacoes = async (data) =>  await this.clientDb.getAllLogNotificacoes(data);

    public postEstoqueLicencas =  async (data) =>  await this.clientDb.postEstoqueLicencas(data);

    public getAllSerialWhitelist = async (data) =>  await this.clientDb.getAllSerialWhitelist(data);

    public postSerialWhite = async (data) =>  await this.clientDb.postSerialWhite(data);
    
    public deleteSerialWhite = async (data) =>  await this.clientDb.deleteSerialWhite(data);

    public getAllLinxResellers = async (data) =>  await this.clientDb.getAllLinxResellers(data);
    
    public getAllCadastroEmail = async (data) =>  await this.clientDb.getAllCadastroEmail(data);
    
    public getAllEstoqueLicensas = async (data) =>  await this.clientDb.getAllEstoqueLicensas(data);
    
    public getByImeiListaMensagens = async (data) =>  await this.clientDb.getByImeiListaMensagens(data);

    public getAllTransacaoVarejista = async (data) =>  await this.clientDb.getAllTransacaoVarejista(data);

    public getAllStatusCompraLicensa = async (data) =>  await this.clientDb.getAllStatusCompraLicensa(data);

    public getAllLinxMovimentoSerial = async (data) =>  await this.clientDb.getAllLinxMovimentoSerial(data);

    public getAllTagsMensagens =  async (data) =>  await this.clientDb.getAllTagsMensagens(data);

    public postListaMensagens = async (data) =>  await this.clientDb.postListaMensagens(data);

    public putListaMensagens = async (data) =>  await this.clientDb.putListaMensagens(data);

    public postUnionMensagensTags = async (data) =>  await this.clientDb.postUnionMensagensTags(data);

    //public putUnionMensagensTags = async (data) =>  await this.clientDb.putUnionMensagensTags(data);

    public getUnionMensagensTags = async (data) =>  await this.clientDb.getUnionMensagensTags(data);

    public postResellerVarejista = async (data) =>  await this.clientDb.postResellerVarejista(data);

    public putResellerVarejista = async (data) =>  await this.clientDb.putResellerVarejista(data);

    public getAllListaMensagens = async (data) =>  await this.clientDb.getAllListaMensagens(data);

    public getAllListaBloqueados = async (data) =>  await this.clientDb.getAllListaBloqueados(data);

    public getAllListaBlinkados = async (data) =>  await this.clientDb.getAllListaBlinkados(data);

    public getAllListaAtivos = async (data) =>  await this.clientDb.getAllListaAtivos(data);

    public getAllListaImeiManual = async (data) =>  await this.clientDb.getAllListaImeiManual(data);

    public getAllListaQuitados = async (data) =>  await this.clientDb.getAllListaQuitados(data);

    public getTransactionId = async (data) =>  await this.clientDb.getTransactionId(data);

    public postCadastroEmail = async (data) =>  await this.clientDb.postCadastroEmail(data);

    public putCadastroEmail = async (data) =>  await this.clientDb.putCadastroEmail(data);

    public deleteCadastroEmail = async (data) =>  await this.clientDb.deleteCadastroEmail(data);

    public postGravaRequest = async (data) =>  await this.clientDb.postGravaRequest(data);

    public postGravaResponse = async (data) =>  await this.clientDb.postGravaResponse(data);

    public putUpdateStatusKg = async (data) =>  await this.clientDb.putUpdateStatusKg(data);

    public getRelacionamentoSoudi = async () =>  await this.clientDb.getRelacionamentoSoudi();

    public putRelacionamentoSoudi = async (data) =>  await this.clientDb.putRelacionamentoSoudi(data);

    public putRemoveMovSerial = async (data) =>  await this.clientDb.putRemoveMovSerial(data);


    public async getOk() {
        let res = await this.client.getOk();
        return res.data;
    }

    public async sendStatus(data: any) {
        let res = await this.client.sendStatus(data);
        return res;
    }

    public async updateStatus(data: any) {
        try {
            let res = await this.client.updateStatus(data);
            return res;
        } catch (err) {
            console.log('Something happened: ' + err);
        }
    }

    public getVendaManual = async (data) =>  { 

        let retorno = new Array<VendaManual>();
        const lista: any[] = await this.clientDb.getVendaManual(data);

        lista.forEach( i => {
            retorno.push(new VendaManual({
                nomeCliente: i.nome_cliente,
                idempresa: i.idempresa,
                cnpj: i.cnpj,
                razaoSocial: i.razao_social,
                nomeLoja: i.nome_fantasia,
                nota: i.nota_fiscal,
                chaveNfe: i.chave_nfe,
                modeloDocumento: i.modelo_nf,
                cpfCliente: i.cpf_cnpj,
                dataEmissao: `${i.data_emissao.getFullYear()}-${i.data_emissao.getMonth()+1}-${i.data_emissao.getDate()}`,
                valor: i.total_liquido,
                formaPagamento: i.nome_plano_cartao_1,
                parcelas: i.parcelas_plano_cartao_1,
                tipoDispositivo: i.categoria,
                imeiSn: i.serial,
                transacao: i.transacao
            }));
        });

        return retorno;
    };

    public postVendaManual = async (data: VendaManual) => {          
        let newData = new Date(data.dataEmissao);                
        let aux = newData.toISOString().slice(0,10).toString();
        data.dataEmissao = aux;

        let venda: VendaManual = new VendaManual(data);            
        let reseller = await this.clientDb.getLinxResselerByCnpj(venda.cnpj);
        if (reseller.length == 0) throw new Error('Varejista não cadastrado, valide seus dados ou solicite inclusão.');
        let imeiValidate = await this.clientDb.getLinxMovimentoSerialBySerial(venda.imeiSn);
        if (imeiValidate.length != 0) throw new Error('IMEI/SN já cadastrados, valide seus dados.');

        venda.idempresa = reseller[0].idempresa;

        if (!venda.numeroVenda) {
            venda.numeroVenda = (await this.clientDb.getNextFaturamento())[0].AUTO_INCREMENT;
        }
  
        let faturamento = new Faturamento(
            venda.nomeCliente,
            venda.idempresa,
            venda.cnpj,
            venda.razaoSocial,
            venda.nomeLoja,
            venda.nota,
            venda.chaveNfe,
            venda.modeloDocumento,
            venda.cpfCliente,
            venda.dataEmissao,
            venda.valor,
            venda.formaPagamento,
            venda.parcelas,
            venda.tipoDispositivo,
            // (await this.clientDb.getNextFaturamento())[0].AUTO_INCREMENT             
            venda.numeroVenda
        );      
        console.log('FATURAMENTO: ', faturamento);            
        await this.clientDb.postVendaManualFaturamento(faturamento);

        let movimentoSerial = new MovimentoSerial(
            venda.cnpj,
            venda.imeiSn,
            faturamento.transacao,
            venda.idempresa,
        );
        await this.clientDb.postVendaManualMovimentoSerial(movimentoSerial);

        return venda;
    }

    public putVendaManual = async (data: VendaManual) => {

        let venda: VendaManual = new VendaManual(data);
        let reseller = await this.clientDb.getLinxResselerByCnpj(venda.cnpj);
        if (reseller.length == 0) throw new Error('Varejista não cadastrado, valide seus dados ou solicite inclusão.');
        let imeiValidate = await this.clientDb.getLinxMovimentoSerialBySerial(venda.imeiSn);
        if (imeiValidate.length != 0 && imeiValidate[0].serial != venda.imeiSn) throw new Error('IMEI/SN já cadastrados, valide seus dados.');
        
        venda.idempresa = reseller[0].idempresa;

        let faturamento = new Faturamento(
            venda.nomeCliente,
            venda.idempresa,
            venda.cnpj,
            venda.razaoSocial,
            venda.nomeLoja,
            venda.nota,
            venda.chaveNfe,
            venda.modeloDocumento,
            venda.cpfCliente,
            venda.dataEmissao,
            venda.valor,
            venda.formaPagamento,
            venda.parcelas,
            venda.tipoDispositivo,
            venda.transacao             
        );
        await this.clientDb.putVendaManualFaturamento(faturamento);

        let movimentoSerial = new MovimentoSerial(
            venda.cnpj,
            venda.imeiSn,
            venda.transacao,
            venda.idempresa
        );
        await this.clientDb.putVendaManualMovimentoSerial(movimentoSerial);

        return venda;
    }
}

export default Service;